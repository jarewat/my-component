/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author jarew
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;
    private int amount;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.amount = 1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + ", amount=" + amount + '}';
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "Americano", 50, "./Image/Americano.png"));
        list.add(new Product(1, "Cappuchino", 45, "./Image/Cappuchino.png"));
        list.add(new Product(1, "Coco", 55, "./Image/Coco.png"));
        list.add(new Product(1, "Latte", 60, "./Image/Latte.png"));
        list.add(new Product(1, "Milk", 50, "./Image/Milk.png"));
        list.add(new Product(1, "Moccha", 45, "./Image/Moccha.png"));
        list.add(new Product(1, "Hot Coco", 50, "./Image/Coco.png"));

        return list;
    }

}
